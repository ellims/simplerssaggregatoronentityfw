use RSS_DataBase

Create procedure InsertFeed
@name nvarchar(100),
@title nvarchar(500),
@description nvarchar(2000),
@url nvarchar(500)
as
Begin
	Insert into Channel values(@name, @title, @description, @url)
End

Create Procedure DeleteFeed
@ID int
as

Begin
	Delete from News where Channel_Id = @ID
	Delete from Channel where Id = @ID
End

Create procedure InsertNews
@title nvarchar(500),
@description nvarchar(2000),
@url nvarchar(500),
@Channel_Id int
as
Begin
	Insert into News values(@title, @description, @url,@Channel_Id)
End

Create Procedure DeleteNews
@ID int
as

Begin
	Delete from News where Id = @ID
End
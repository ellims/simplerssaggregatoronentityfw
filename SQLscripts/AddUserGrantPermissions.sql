use RSS_DataBase

CREATE LOGIN [IIS APPPOOL\ASP.NET v4.0 Classic] 
  FROM WINDOWS WITH DEFAULT_DATABASE=[master], 
  DEFAULT_LANGUAGE=[us_english]
GO
CREATE USER [RssAggregatorUser] 
  FOR LOGIN [IIS APPPOOL\ASP.NET v4.0 Classic]
GO
 EXEC sp_addrolemember 'db_owner', 'RssAggregatorUser'
GO
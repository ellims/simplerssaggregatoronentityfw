﻿using SImpleRSSAggregator.Web.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace SImpleRSSAggregator.Web
{
    public static  class RssDataBaseClient
    {
        /// <summary>
        /// Adds a new feed channet to database
        /// </summary>
        /// <param name="feed"></param>
        /// <returns></returns>
        public static string AddFeed(RSSFeed feed)
        {
            try
            {
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();
                context.InsertFeed(feed.Name, feed.Title, feed.Description, feed.URL);
                int lastindex = context.Channels.ToList().Count - 1;
                feed.ID = context.Channels.ToList()[lastindex].Id;
                foreach (RSSNewsItem news in feed.news)
                    context.InsertNews(news.Title, news.Description, news.URL, feed.ID);
                context.SaveChanges();
                return "New feed channel with " + feed.news.Count + " news was added successfully";
            }

            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Gets all news of a concrete feed channel from database
        /// </summary>
        /// <param name="channel_ID"></param>
        /// <returns></returns>
        public static RSSNews GetFeedNews(int channel_ID)
        {
            try
            {
                RSSNews rssNews = new RSSNews();
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();
                var feednews = from news in context.News where news.Channel_Id == channel_ID select news;
                foreach (var n in feednews)
                    rssNews.Add(new RSSNewsItem()
                        {
                            Channel_ID = n.Channel_Id,
                            Title = n.title,
                            Description = n.description,
                            ID = n.Id,
                            URL = n.URL
                        });

                return rssNews;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets all news from database
        /// </summary>
        /// <returns></returns>
        public static RSSNews GetAllNews()
        {
            try
            {
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();

                RSSNews rssNews = new RSSNews();
                foreach (var n in context.News.ToList())
                    rssNews.Add(new RSSNewsItem()
                    {
                        Channel_ID = n.Channel_Id,
                        Title = n.title,
                        Description = n.description,
                        ID = n.Id,
                        URL = n.URL
                    });
                return rssNews;
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets all feed channels from the database
        /// </summary>
        /// <returns></returns>
        public static List<RSSFeed> GetAllChannels()
        {
            try
            {
                List<RSSFeed> rssFeeds = new List<RSSFeed>();
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();
                //return context.Channels.ToList();
                foreach (Channel ch in context.Channels.ToList())
                {
                    rssFeeds.Add(new RSSFeed()
                                    {
                                        Description = ch.description,
                                        ID = ch.Id,
                                        Name = ch.name,
                                        Title = ch.title,
                                        URL = ch.URL,
                                        news = new RSSNews(ch.News)
                                    });
                }
                return rssFeeds;
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Removes a concrete news from the database
        /// </summary>
        /// <param name="news_ID"></param>
        public static void RemoveNews(int news_ID)
        {
            try
            {
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();
                context.DeleteNews(news_ID);
                context.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Removes a concrete feed channel from the database
        /// </summary>
        /// <param name="channel_ID"></param>
        public static void RemoveChannel(int channel_ID)
        {
            try
            {
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();
                context.DeleteFeed(channel_ID);
                context.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Removes all feed channels from the database
        /// </summary>
        public static void RemoveAllNews()
        {
            try
            {
                RSS_DataBaseEntities context = new RSS_DataBaseEntities();
                for (int i = 0; i < context.Channels.ToList().Count; i++ )
                        context.DeleteFeed(context.Channels.ToList()[i].Id);
                context.SaveChanges();
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using SImpleRSSAggregator.Web.Model;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

namespace SImpleRSSAggregator.Web
{
    /// <summary>
    /// Describes single news in RSS feed
    /// </summary>
    [DataContract]
    public class RSSNewsItem
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string URL { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int Channel_ID { get; set; }

        public RSSNewsItem() { }
        public RSSNewsItem(XmlNode node)
        {
            if (node != null)
            {
                foreach (XmlNode innerNode in node)
                {
                    switch (innerNode.Name)
                    {
                        case "title": this.Title = innerNode.InnerText; break;
                        case "link": this.URL = innerNode.InnerText; break;
                        case "description": this.Description = innerNode.InnerText; break;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Contains news of RSS feed
    /// </summary>
    [CollectionDataContract]
    public class RSSNews : List<RSSNewsItem>
    {
        /// <summary>
        /// Check the news for the uniqueness
        /// </summary>
        /// <param name="itemToCheck"></param>
        /// <returns></returns>

        
        public new bool Contains(XmlNode itemToCheck)
        {
            if (itemToCheck != null)
                foreach (XmlNode innerNode in itemToCheck)
                {
                    if (innerNode.Name == itemToCheck.Name && innerNode.InnerText == itemToCheck.InnerText)
                    {
                        return true;
                    }
                }
            return false;
        }

        public RSSNews() { }
        public RSSNews(ICollection<News> coll)
        {
            foreach (News n in coll)
                this.Add(new RSSNewsItem()
                                {
                                    Channel_ID = n.Channel_Id,
                                    Description = n.description,
                                    ID = n.Id,
                                    Title = n.title,
                                    URL = n.URL
                                });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Xml;
using SImpleRSSAggregator.Web.Model;
using System.Linq;
using System.Text;


using System.Runtime.Serialization;





namespace SImpleRSSAggregator.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RssAggregatorService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RssAggregatorService.svc or RssAggregatorService.svc.cs at the Solution Explorer and start debugging.
    public class RssAggregatorService : IRssAggregatorService
    {

        /// <summary>
        /// Adds new channel to DataBase 
        /// </summary>
        /// <param name="url">Source URL of added RssChannel</param>
        /// <param name="name">User`s name of added RssChannel</param>
        /// <returns>Returns a string with a number of news in channel</returns>
        public string AddFeed(string url, string name)
        {
            try
            {
                RSSFeed feed = new RSSFeed(url, name);
                return RssDataBaseClient.AddFeed(feed);
            }

            catch (XmlException ex)
            {
                XmlExceptDetails details = new XmlExceptDetails(ex);
                throw new FaultException<XmlExceptDetails>(details, new FaultReason( "Add Feed Failed:\r\n" + ex.Message));
            }

            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason("Add Feed Failed:\r\n" + ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason("Add Feed Failed:\r\n" + ex.Message));
            }
        }

        /// <summary>
        /// Removes channel from DataBase by "Id"  ***********   
        /// </summary>
        /// <param name="feed_Id">feed`s ID in database</param>
        public void RemoveFeed(int feed_Id)
        {
            try
            {
                RssDataBaseClient.RemoveChannel(feed_Id);
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Gets all channels from database  
        /// </summary>
        /// <returns>the list of channels from database</returns>
        public List<RSSFeed> GetFeeds()
        {
            try
            {
            List<RSSFeed> retlist = RssDataBaseClient.GetAllChannels();
            return retlist;
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Returns all news from all feeds     *************
        /// </summary>
        /// <returns></returns>
        public RSSNews FetchAllFeedNews()
        {
            try
            {
                return RssDataBaseClient.GetAllNews();
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Returns news from concrete RSS feed     ************
        /// </summary>
        /// <param name="channel_id">rssChannel`s Id in database</param>
        /// <returns></returns>
        public RSSNews GetFeedNews(int channel_id)
        {
            try
            {
                return RssDataBaseClient.GetFeedNews(channel_id);
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Removes single news by unique ID        ***********
        /// </summary>
        /// <param name="item_id">news`s id in database</param>
        public void RemoveNews(int item_id)
        {
            try
            {
                RssDataBaseClient.RemoveNews(item_id);
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Removes all saved feeds from application    ********
        /// </summary>
        public void RemoveAllNews()
        {
            try
            {
                RssDataBaseClient.RemoveAllNews();
            }
            catch (SqlException ex)
            {
                SqlExceptDetails details = new SqlExceptDetails(ex);
                throw new FaultException<SqlExceptDetails>(details, new FaultReason(ex.Message));
            }

            catch (Exception ex)
            {
                SomeExceptDetails details = new SomeExceptDetails(ex);
                throw new FaultException<SomeExceptDetails>(details, new FaultReason(ex.Message));
            }
        }
    }
}

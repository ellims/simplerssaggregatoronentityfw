﻿using System;
using System.Windows;
using System.Windows.Controls;
using SImpleRSSAggregator.RssAggregatorService;

namespace SImpleRSSAggregator.ChildWindows
{
    public partial class AddWindow : ChildWindow
    {
        public AddWindow()
        {
            InitializeComponent();
        }

        public AddWindow(MainPage page)
        {
            InitializeComponent(); 
            this.page = page; 
        }

        MainPage page;

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            RssAggregatorServiceClient client = new RssAggregatorServiceClient();
            try
            {
                client.OpenAsync();
                client.AddFeedCompleted += client_AddFeedCompleted;
                client.AddFeedAsync(UrlTextBox.Text, NameTextBox.Text);
            }

            catch (Exception ex)
            {
                ShowMessage("Exception!", ex.Message);
            }

            finally
            {
                client.CloseAsync();
            }
            this.DialogResult = true;
        }

        private void client_AddFeedCompleted(object sender, AddFeedCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                ShowMessage("Error!", "Adding wasn`t completed!\r\n" + e.Error.Message);
            }

            if (e.Result.Contains("news was added"))
            {
                ShowMessage("Complete!", "Adding was completed successfully!\r\n" + e.Result);
            }

            else
            {
                ShowMessage("Error!", "Adding wasn`t completed!\r\n" + e.Result);
            }
        }

        /// <summary>
        /// Displays result window
        /// </summary>
        /// <param name="title">Message Header </param>
        /// <param name="message"> Message text </param>
        private void ShowMessage(string title, string message)
        {
            var completeMessage = new MessageWindow();
            completeMessage.CancelButton.Visibility = System.Windows.Visibility.Collapsed;
            completeMessage.Title = title;
            completeMessage.MessageTextBlock.Text = message;
            this.Close();
            page.UpdateForm();
            completeMessage.Show();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}


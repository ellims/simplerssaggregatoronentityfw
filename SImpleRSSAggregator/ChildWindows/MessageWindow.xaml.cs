﻿using System.Windows;
using System.Windows.Controls;
using SImpleRSSAggregator.RssAggregatorService;

namespace SImpleRSSAggregator.ChildWindows
{
    public partial class MessageWindow : ChildWindow
    {
        public MessageWindow()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.MessageTextBlock.Text.Contains("delete\r\n all saved feeds"))
            {
                RssAggregatorServiceClient client = new RssAggregatorServiceClient();
                client.OpenAsync();
                client.RemoveAllNewsAsync();
                client.CloseAsync();
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

    }
}


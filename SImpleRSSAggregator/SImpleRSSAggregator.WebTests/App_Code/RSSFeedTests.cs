﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SImpleRSSAggregator.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.IO;

namespace SImpleRSSAggregator.Web.Tests
{
    [TestClass()]
    public class RSSFeedTests
    {      
        [TestMethod()]
        public void RSSFeedTestFeedDescription()
        {
            string expected = "TestFeedDescription";
            RSSFeed feed = new RSSFeed(@"testfile.xml", "Test");
            string actual = feed.Description;
            Assert.AreEqual(expected,actual);
        }

        [TestMethod()]
        public void RSSFeedTestFeedTitle()
        {
            string expected = "TestFeedTitle";
            RSSFeed feed = new RSSFeed(@"testfile.xml", "Test");
            string actual = feed.Title;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RSSFeedTestFeedURL()
        {
            string expected = "http://testlink.feed";
            RSSFeed feed = new RSSFeed(@"testfile.xml", "Test");
            string actual = feed.URL;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RSSFeedTestNewsDescription()
        {
            string expected = "Test NewsItem Description";
            RSSFeed feed = new RSSFeed(@"testfile.xml", "Test");
            string actual = feed.news[0].Description;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RSSFeedTestNewsTitle()
        {
            string expected = "TestNewsTitle";
            RSSFeed feed = new RSSFeed(@"testfile.xml", "Test");
            string actual = feed.news[0].Title;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void RSSFeedTestNewsURL()
        {
            string expected = "http://testlink.newsItem";
            RSSFeed feed = new RSSFeed(@"testfile.xml", "Test");
            string actual = feed.news[0].URL;
            Assert.AreEqual(expected, actual);
        }

    }
}
